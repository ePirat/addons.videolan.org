--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: addonstatus; Type: TYPE; Schema: public; Owner: fcartegn
--

CREATE TYPE addonstatus AS ENUM (
    'invalid',
    'waiting',
    'valid',
    'updated',
    'junk'
);


ALTER TYPE public.addonstatus OWNER TO fcartegn;

--
-- Name: addontype; Type: TYPE; Schema: public; Owner: fcartegn
--

CREATE TYPE addontype AS ENUM (
    'extension',
    'skin',
    'playlist',
    'other',
    'discovery',
    'interface',
    'meta'
);


ALTER TYPE public.addontype OWNER TO fcartegn;

--
-- Name: entries_updatesize_tr(); Type: FUNCTION; Schema: public; Owner: addons
--

CREATE FUNCTION entries_updatesize_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    c_uuid uuid;
BEGIN

IF (TG_OP = 'DELETE') THEN
    c_uuid = OLD.ref_uuid;
ELSE
    c_uuid = NEW.ref_uuid;
END IF;

UPDATE entries SET size = total.v FROM
(
    SELECT SUM(octet_length(data)) AS v FROM files WHERE ref_uuid = c_uuid
) AS total
WHERE uuid = c_uuid;
RETURN NULL;
END;
$$;


ALTER FUNCTION public.entries_updatesize_tr() OWNER TO addons;

--
-- Name: set_user_role_tr(); Type: FUNCTION; Schema: public; Owner: addons
--

CREATE FUNCTION set_user_role_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

INSERT INTO user_role_linker (user_id,role_id) VALUES (NEW.user_id, 1);

RETURN NULL;

END;
$$;


ALTER FUNCTION public.set_user_role_tr() OWNER TO addons;

--
-- Name: update_status_tr(); Type: FUNCTION; Schema: public; Owner: addons
--

CREATE FUNCTION update_status_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
    c_uuid uuid;
    v_count integer;
BEGIN

IF (TG_OP = 'DELETE') THEN
    c_uuid = OLD.ref_uuid;
ELSE
    c_uuid = NEW.ref_uuid;
END IF;

SELECT COUNT(*) INTO v_count FROM files WHERE ref_uuid = c_uuid;

IF v_count = 0
THEN
    UPDATE entries SET status = 'invalid' WHERE uuid = c_uuid;
ELSE
    UPDATE entries SET status = 'waiting' WHERE uuid = c_uuid;
END IF;


RETURN NULL;
END;$$;


ALTER FUNCTION public.update_status_tr() OWNER TO addons;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: entries; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE entries (
    seq integer NOT NULL,
    uuid uuid NOT NULL,
    name character varying(126) NOT NULL,
    image character varying(1024),
    type addontype DEFAULT 'other'::addontype,
    score real DEFAULT 0,
    downloads integer DEFAULT 0,
    description text,
    summary character varying(255),
    archive character varying(255),
    imagedata bytea,
    version character varying(10),
    ref_userid integer,
    size integer,
    votes integer DEFAULT 0,
    status addonstatus DEFAULT 'invalid'::addonstatus
);


ALTER TABLE public.entries OWNER TO addons;

--
-- Name: entries_seq_seq; Type: SEQUENCE; Schema: public; Owner: addons
--

CREATE SEQUENCE entries_seq_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entries_seq_seq OWNER TO addons;

--
-- Name: entries_seq_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: addons
--

ALTER SEQUENCE entries_seq_seq OWNED BY entries.seq;


--
-- Name: files; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE files (
    ref_uuid uuid NOT NULL,
    type addontype,
    filename character varying(100) NOT NULL,
    data bytea
);


ALTER TABLE public.files OWNER TO addons;

--
-- Name: user; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE "user" (
    user_id integer NOT NULL,
    username character varying(255) DEFAULT NULL::character varying,
    email character varying(255) DEFAULT NULL::character varying,
    display_name character varying(50) DEFAULT NULL::character varying,
    password character varying(128) NOT NULL,
    state smallint
);


ALTER TABLE public."user" OWNER TO addons;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE user_role (
    id integer NOT NULL,
    role_id character varying(255) NOT NULL,
    is_default smallint DEFAULT 0 NOT NULL,
    parent_id integer
);


ALTER TABLE public.user_role OWNER TO addons;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: addons
--

CREATE SEQUENCE user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_id_seq OWNER TO addons;

--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: addons
--

ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;


--
-- Name: user_role_linker; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE user_role_linker (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.user_role_linker OWNER TO addons;

--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: addons
--

CREATE SEQUENCE user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_id_seq OWNER TO addons;

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: addons
--

ALTER SEQUENCE user_user_id_seq OWNED BY "user".user_id;


--
-- Name: seq; Type: DEFAULT; Schema: public; Owner: addons
--

ALTER TABLE ONLY entries ALTER COLUMN seq SET DEFAULT nextval('entries_seq_seq'::regclass);


--
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: addons
--

ALTER TABLE ONLY "user" ALTER COLUMN user_id SET DEFAULT nextval('user_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);


--
-- Name: combined; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT combined PRIMARY KEY (ref_uuid, filename);


--
-- Name: entries_pkey; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT entries_pkey PRIMARY KEY (uuid);


--
-- Name: user_email_key; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- Name: user_role_id_key; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_id_key UNIQUE (id);


--
-- Name: user_role_linker_pkey; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY user_role_linker
    ADD CONSTRAINT user_role_linker_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: idx_parent_id; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE INDEX idx_parent_id ON user_role USING btree (parent_id);


--
-- Name: idx_role_id; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE INDEX idx_role_id ON user_role_linker USING btree (role_id);


--
-- Name: idx_user_id; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE INDEX idx_user_id ON user_role_linker USING btree (user_id);


--
-- Name: unique_role; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE UNIQUE INDEX unique_role ON user_role USING btree (role_id);


--
-- Name: entries_updatesize; Type: TRIGGER; Schema: public; Owner: addons
--

CREATE TRIGGER entries_updatesize AFTER INSERT OR DELETE OR UPDATE ON files FOR EACH ROW EXECUTE PROCEDURE entries_updatesize_tr();


--
-- Name: entries_updatestatus; Type: TRIGGER; Schema: public; Owner: addons
--

CREATE TRIGGER entries_updatestatus AFTER INSERT OR DELETE OR UPDATE ON files FOR EACH ROW EXECUTE PROCEDURE update_status_tr();


--
-- Name: set_user_role_trigger; Type: TRIGGER; Schema: public; Owner: addons
--

CREATE TRIGGER set_user_role_trigger AFTER INSERT ON "user" FOR EACH ROW EXECUTE PROCEDURE set_user_role_tr();


--
-- Name: fk_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_parent_id FOREIGN KEY (parent_id) REFERENCES user_role(id) ON DELETE SET NULL;


--
-- Name: fk_role_id; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role_linker
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES user_role(id) ON DELETE CASCADE;


--
-- Name: fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role_linker
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES "user"(user_id) ON DELETE CASCADE;


--
-- Name: refuid; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY entries
    ADD CONSTRAINT refuid FOREIGN KEY (ref_userid) REFERENCES "user"(user_id) ON DELETE CASCADE;


--
-- Name: refuuid; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY files
    ADD CONSTRAINT refuuid FOREIGN KEY (ref_uuid) REFERENCES entries(uuid) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

