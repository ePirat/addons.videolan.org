<?php

namespace Addons\Model;
use \ZipArchive;
use \DOMDocument;
use \DOMXpath;
use \Exception;
use Addons\Filter\PNGResizer;

class Archive
{
    protected $vlp;
    protected $files;

    public function __construct()
    {
        $this->vlp = new ZipArchive;
    }

    protected function buildManifest( $data )
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->formatOutput = true; 
        $root = $dom->createElementNS( 'http://videolan.org/ns/vlc/addons/1.0', 'videolan' );
        $desc = $dom->createElement( "addons" );

        unset( $data['score'] );
        unset( $data['downloads'] );

        $addonnodehydrator = new \Addons\Mapper\XMLAddonNodeHydrator;
        $addonnode = $dom->createElement("addon");
        $addonnodehydrator->hydrate($data, $addonnode);
        $desc->appendChild($addonnode);

        $root->appendChild( $desc );
        $dom->appendChild( $root );

        return $dom->saveXML();
    }

    public function pack( $filename, $data )
    {
        $i = $this->vlp->open( $filename, ZipArchive::CREATE | ZipArchive::OVERWRITE );
        $manifest = $this->buildManifest( $data );
        $this->vlp->addFromString("manifest.xml", $manifest);
        foreach ( $data['resources'] as $filedata )
        {
            if ( is_resource( $filedata['data'] ) )
                $this->vlp->addFromString( $filedata['filename'], stream_get_contents($filedata['data']) );
            else
                $this->vlp->addFromString( $filedata['filename'], $filedata['data'] );
        }
        $this->vlp->close();
    }
    
    public function unpack( $filename )
    {
        $this->vlp->open($filename);

        for( $i = 0; $i < $this->vlp->numFiles; $i++ )
        {
            $stat = $this->vlp->statIndex( $i );
            $this->files[ $stat['name'] ] = $stat;
        }

        if ( empty($this->files['manifest.xml']) )
            throw new Exception("Manifest not found in archive");

        $dom = new DOMDocument();
        $dom->loadXML( $this->vlp->getFromName('manifest.xml') );
        $xpath = new DOMXpath( $dom );

        /* Register the videolan namespace */
        $rootNamespace = $dom->lookupNamespaceUri( $dom->namespaceURI );
        $xpath->registerNamespace( 'vo', $rootNamespace ); 

        $nodes = $xpath->query("/vo:videolan");
        if ( $nodes->length == 0 )
            throw new Exception("Non videolan manifest");

        $nodes = $xpath->query("/vo:videolan/vo:addons/vo:addon");
        if ( $nodes->length != 1 )
            throw new Exception("Missing or non unique addon");

        $addonnode = $nodes->item(0);

        $hydrator = new \Addons\Mapper\XMLAddonNodeHydrator();
        $data = $hydrator->extract($addonnode);

        $addon = new \Addons\Model\Addon();
        $addon->exchangeArray( $data );

        foreach( $data['resources'] as $filedata )
        {
            if ( empty($this->files[$filedata['filename']]) )
               throw new Exception($filedata['filename'] . "declared in Manifest but not found in archive");
            else
                $filedata['data'] = $this->vlp->getFromName( $filedata['filename'] );
            $addonfile = new AddonFile();
            $addonfile->exchangeArray($filedata);
            $addon->resources[] = $addonfile;
        }

        if ( !empty($this->files['thumbnail.png']) )
        {
            $thumbnail = $this->vlp->getFromName( 'thumbnail.png' );
            $resizer = new PNGResizer( array( 'width' => 256, 'height' => 256 ));
            $addon->imagedata = $resizer->filter($thumbnail);
        }

        $addon->validateAll();

        return $addon;
    }
}

