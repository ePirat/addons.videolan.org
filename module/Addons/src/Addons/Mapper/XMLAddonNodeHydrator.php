<?php

namespace Addons\Mapper;

use Zend\Stdlib\Hydrator\HydratorInterface;
use \DOMNode;
use \DOMXPath;
use \Exception;
use Addons\Model\Addon;

/**
 * Description of AddonHydrator
 *
 * @author fcartegn
 */
class XMLAddonNodeHydrator implements HydratorInterface
{
 
    public function extract($addonnode)
    {
        $result = Array();
        if ( !$addonnode instanceof DOMNode )
            return $result;

        foreach ( array("type", "id", "score", "downloads", "version") as $type )
        {
            switch( $type )
            {
                case "id":
                    $result['uuid'] = trim($addonnode->getAttribute($type));
                    $validator = Addon::getValidator('uuid');
                    if ( $validator && ! $validator->isValid( $result['uuid'] ) )
                        throw new Exception( $type . ":" . array_values( $validator->getMessages() )[0] );
                    break;
                case "type":
                    $result[$type] = trim($addonnode->getAttribute($type));
                    $validator = Addon::getValidator($type);
                    if ( $validator && ! $validator->isValid( $result[$type] ) )
                        throw new Exception( $type . ":" . array_values( $validator->getMessages() )[0] );
                    break;
//                case "score":
//                case "downloads":
                case "version":
                    $result[$type] = $addonnode->getAttribute($type);
                    $validator = Addon::getValidator('version');
                    if ( $validator && ! $validator->isValid( $result[$type] ) )
                         throw new Exception( $type . ":" . array_values( $validator->getMessages() )[0] );
                    break;
                default:
                    break;
            }
        }

        $xpath = new DOMXPath( $addonnode->ownerDocument );
        $xpath->registerNamespace( 'vo', $addonnode->namespaceURI );

        $nodes = $xpath->query( "vo:image|vo:name|vo:description|vo:summary", $addonnode );
        foreach( $nodes as $node )
        {
            switch( $node->nodeName )
            {
                case "image":
                    $result['imagedata'] = base64_decode( $node->nodeValue );
                    break;
                default:
                    $result[$node->nodeName] = $node->nodeValue;
            }
        }

        $names = array("creator", "email", "sourceurl");
        $nodes = $xpath->query("vo:authorship/*", $addonnode);
        foreach ($nodes as $node)
            if (in_array($node->nodeName, $names))
                $result[$node->nodeName] = $node->nodeValue;

        $resources = $xpath->query( "vo:resource", $addonnode );
        foreach( $resources as $node )
        {
            $file = array();
            
            $validator = Addon::getValidator('type');
            $file['type'] = trim($node->getAttribute('type'));
            if ( $validator && ! $validator->isValid( $file['type'] ) )
               throw new Exception( 'type' . ":" . array_values( $validator->getMessages() )[0] );

            $validator = Addon::getValidator('filename');
            $file['filename'] = trim($node->nodeValue);
            if ( $validator && ! $validator->isValid( $file['filename'] ) )
               throw new Exception( 'filename' . ":" . array_values( $validator->getMessages() )[0] );

            $file['ref_uuid'] = $result['uuid'];

            if ( !isset( $result['resources'] ) )
                $result['resources'] = array();
            $result['resources'][] = $file;
        }
        
        return $result;
    }

    public function hydrate(array $data, $addonnode)
    {
        if ( !$addonnode instanceof \DOMElement )
            return;

        $dom = $addonnode->ownerDocument;

        foreach ( array("type", "uuid", "score", "downloads", "version") as $type )
        {
            if ( isset( $data[$type] ) )
            {
                switch( $type )
                {
                    case "uuid":
                        $addonnode->setAttribute('id', $data[$type]);
                        break;
                    case "score":
                        $addonnode->setAttribute($type, number_format( $data[$type] * 100, 0, '', '' ) );
                        break;
                    default:
                        $addonnode->setAttribute($type, $data[$type]);
                }
            }
        }

        $addonnode->appendChild($dom->createElement("name", $data['name']));
        $addonnode->appendChild($dom->createElement("summary", $data['summary']));

        $description = $dom->createElement("description");
        $description->appendChild($dom->createCDATASection($data['description']));
        $addonnode->appendChild($description);

        if (!empty($data['imagedata']))
        {
            if (is_resource($data['imagedata']))
                $imagedata = stream_get_contents( $data['imagedata'] );
            else
                $imagedata = $data['imagedata'];

            $resizer = new \Addons\Filter\PNGResizer( array( 'width' => 48, 'height' => 48 ));
            $imagedata = $resizer->filter( $imagedata );
            $imagedata = base64_encode( $imagedata );

            $imagenode = $dom->createElement("image", $imagedata);
            $imagenode->setAttribute('format', 'png');
            $addonnode->appendChild($imagenode);
        }

        $auth = $dom->createElement("authorship");
        $auth->appendChild($dom->createElement("creator", isset($data['creator']) ? $data['creator'] : "Unknown" ));
        $auth->appendChild($dom->createElement("email", isset($data['email']) ? $data['email'] : "" ));
        $auth->appendChild($dom->createElement("sourceurl", ""));
        $addonnode->appendChild($auth);

        foreach( $data['resources'] as $filedata )
        {
            $resource = $dom->createElement( "resource", $filedata['filename'] );
            $resource->setAttribute( 'type', $filedata['type'] );
            $addonnode->appendChild( $resource );
        }
        
    }    //put your code here
}
