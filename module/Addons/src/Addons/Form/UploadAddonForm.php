<?php

namespace Addons\Form;

use Zend\Form\Element;
use ZfcBase\Form\ProvidesEventsForm;
use Zend\Filter;
use Zend\InputFilter\Input;
use Zend\InputFilter\FileInput;

class UploadAddonForm extends ProvidesEventsForm
{
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'fileupload',
            'options' => array(
                'label' => 'File',
            ),
            'attributes' => array(
                'type' => 'file'
            ),
        ));

        $input = new FileInput('fileupload');
        $input->getValidatorChain()->addValidator( new \Zend\Validator\File\UploadFile() );
        $input->setRequired( true );
        $this->getInputFilter()->add( $input );
        
        $submitElement = new Element\Button('submit');
        $submitElement
            ->setLabel('Upload')
            ->setAttributes(array(
                'type'  => 'submit',
            ));

        $this->add($submitElement, array(
            'priority' => -100,
        ));

    }
}
